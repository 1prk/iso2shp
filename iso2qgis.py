###Isochronen-Skript für QGIS auf Grundlage der Graphhopper-API
###erstellt von iwpo
###Version 0.2
###TODO: simple Aufforderung (in Form von Popups z.B.) zur Parametereingabe

import requests
from qgis.core import QgsVectorLayer, QgsFeature, QgsGeometry, QgsProject
from shapely.geometry import shape

###Koordinaten für die Erzeugung von Isochronen werden im QGIS auf der Karte über Rechtsklick -> Koordinaten kopieren -> WGS84 bezogen
###Hier werden die Koordinaten aus der Zwischenablage hinzugefügt, z.B.
###paste = 13.011567,52.401097
paste = 13.011567,52.401097

###Zusätzliche Parameter für die Isochronenberechnung werden hier eingestellt
###Hinweis: ÖV-Isochronen können nur im VBB-Bereich (Berlin-Brandenburg) berechnet werden (Stand: 13.8.2021)
###time: Zeit in Minuten
###mode: Verkehrsmittel ("car", "foot", "bike", "pt")
###öv_zeit: Abfahrtszeit, auf die sich die ÖV-Isochronenberechnung bezieht
###Die Zeit wird im folgenden Format eingegeben: YYYY-MM-DDTHH:MM:SS.SSSZ
###Beispiel für den 12.8.2021 um 12.30 Uhr:
###öv_zeit = "2021-08-12T12:30:00.000Z"

time = 30
mode = "car"
öv_zeit = "2021-07-07T11:45:00.000Z"

###Weitere Parameter, die für das Funktionieren dieses Skripts notwendig sind
#
#
#
#
#
#


lon = str(paste[1])
lat = str(paste[0])
buckets = 1
reverse_flow = "false"
coords = (lon+','+lat)


print(coords)
url = "http://185.183.159.250:8989/isochrone"
payload = {"point": str(coords),
            "time_limit": time*60,
            "buckets": buckets,
            "vehicle": mode,
            "reverse_flow": reverse_flow,
            "pt.earliest_departure_time": öv_zeit}
r = requests.get(url, params=payload)
gjsn = r.json()

layer = QgsVectorLayer('Polygon?crs=epsg:4326', mode+'_'+str(time)+'min_'+coords, 'memory')
pr = layer.dataProvider()
poly = QgsFeature()

ptl = QgsVectorLayer("Point", mode+'_'+str(time)+'min_'+coords, "memory")
prr = ptl.dataProvider()
point = QgsFeature()
point.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(float(lat),float(lon))))
prr.addFeatures([point])
ptl.updateExtents()


shp = shape(gjsn['polygons'][0]['geometry'])
geom = QgsGeometry.fromWkt(str(shp))
poly.setGeometry(geom)
pr.addFeatures([poly])
layer.setOpacity(0.5)
layer.updateExtents()

QgsProject.instance().addMapLayers([layer])
QgsProject.instance().addMapLayers([ptl])
