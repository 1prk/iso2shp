import io
import sys
from pyqtlet import L, MapWidget
import geopandas as gpd
import folium
import geocoder
import requests

from folium.plugins import Draw, MousePosition
from PyQt5 import QtWidgets, uic, QtWebEngineWidgets
from PyQt5.QtWidgets import QLineEdit, QLabel, QPushButton, QSlider, QComboBox, QApplication, QVBoxLayout, QWidget
from PyQt5.QtCore import QObject
from PyQt5.QtWebEngineWidgets import QWebEngineView

def isochrone(lat,lon,time,vehicle,buckets,weighting):

    coordinate_list = [str(lat).replace(' ', ''), str(lon).replace(' ', '')]
    coordinate_str = ','.join(coordinate_list)
    payload = {'point': coordinate_str,
               'time_limit': time,
               'vehicle': vehicle,
               'buckets': buckets,
               'weighting': weighting
               }
    r = requests.get('http://185.183.159.250:8989/isochrone', params=payload)
    q = r.json()
    gdf = gpd.GeoDataFrame.from_features(q["polygons"])
    gdf_crs = gdf.set_crs(epsg='4326')

    return gdf_crs

def geo2coords(loc):
    l_obj = geocoder.osm(loc)
    l = l_obj.json
    coords = [l['lat'], l['lng']]
    return coords

class MapWindow(QWidget):
    def __init__(self):

        # Setting up the widgets and layout
        super().__init__()

        self.setGeometry(60, 60, 800, 600)

        self.mapWidget = MapWidget()
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.mapWidget)
        self.setLayout(self.layout)

        # Working with the maps with pyqtlet
        self.map = L.map(self.mapWidget)
        self.map.setView([51.335, 12.320], 10)
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(self.map)
        self.map.clicked.connect(self.mouseCoords)

        #lineedit
        self.nameLabel = QLabel(self)
        self.nameLabel.setText('Latitiude:')
        self.lat = QLineEdit(self)
        self.lon = QLineEdit(self)

        self.lat.move(600, 20)
        self.lat.resize(200, 32)
        #self.lat.setText(self.map.clicked.connect(lambda y: y['latlng']['lat']))
        self.nameLabel.move(20, 20)

        self.lon.move(600, 60)
        self.lon.resize(200, 32)
        #self.lat.setText(self.map.clicked.connect(lambda x: x['latlng'][1]))




        self.show()

    def mouseCoords(self, value):
        lat = value['latlng']['lat']
        lon = value['latlng']['lng']
        coords = [lat, lon]
        print(coords)
        self.iso = L.geoJson(isochrone(coords[0], coords[1], 3000, 'foot', 1, 'short_fastest')).addTo()
        self.map.addLayer(self.iso)
        return coords

class WebEnginePage(QtWebEngineWidgets.QWebEnginePage):
    def javaScriptConsoleMessage(self, level, message, line, sourceID):
        coords_dict = json.loads(message)
        coords = coords_dict['geometry']['coordinates'][0]
        print(coords)
        return coords

#QtWidgets.QMainWindow
class Ui(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        uic.loadUi('map.ui', self)

        m = folium.Map(location=(51.335,12.320), tiles='openstreetmap', zoom_start=13)

        MousePosition(
            position='topright',
            separator=' | ',
            empty_string='NaN',
            lng_first=True,
            num_digits=20,
            prefix='Coordinates:'
        ).add_to(m)

        draw = Draw(
            draw_options={
                'polyline': False,
                'rectangle': False,
                'polygon': False,
                'circle': False,
                'marker': True,
                'circlemarker': False
            }
        )
        m.add_child(draw)
        data = io.BytesIO()
        m.save(data, close_file=False)
        m.save("test.html")
        self.widget.setHtml(data.getvalue().decode())


        self.pushButton.clicked.connect(self.onButtonClicked)
        self.timeLimit.setRange(60, 3000)
        self.timeLimit.setSingleStep(60)
        self.timeLimit.valueChanged.connect(self.updateLabel)

        self.buckets.setRange(1,10)
        self.buckets.setSingleStep(1)
        self.buckets.valueChanged.connect(self.updateLabel)

        self.modeChoice.addItem('foot')
        self.modeChoice.addItem('bike')
        self.modeChoice.addItem('car')
        #self.modeChoice.activated[str].connect(self.chooseMode)

        self.geoparser.clicked.connect(self.geoButton)

        self.show()



    def geoButton(self):
        loc = self.geoCoder.text()
        lat = geo2coords(loc)[0]
        lon = geo2coords(loc)[1]
        self.lineEdit.setText(str(lat))
        self.lineEdit_2.setText(str(lon))
        print(lat, lon)
        return lat, lon

    def updateLabel(self, value):
        self.label_3.setText(str(value))
        print(value)
        return value

    def onButtonClicked(self):
        lat = self.lineEdit.text()
        lon = self.lineEdit_2.text()
        range = self.timeLimit.value()
        buckets = self.buckets.value()
        mode = self.modeChoice.currentText()
        coords = (lat, lon)
        print(range)
        print(mode)
        #coordinate = (51.335894, 12.320652)
        m = folium.Map(location=coords, tiles='openstreetmap', zoom_start=13)
        folium.Marker(coords).add_to(m)
        poly = folium.features.GeoJson(isochrone(coords[0], coords[1], range, mode, buckets, 'short_fastest'))
        m.add_child(poly)
        m.add_child(folium.LatLngPopup())

        data = io.BytesIO()
        m.save(data,close_file=False)

        # leaflet-map in qt-widget
        self.widget.setHtml(data.getvalue().decode())




if __name__ == '__main__':
    #isochrone(51.335894, 12.320652, 6000, 'foot', 2, 'short_fastest')

    app = QtWidgets.QApplication(sys.argv)
    view = QtWebEngineWidgets.QWebEngineView()
    page = WebEnginePage(view)
    window = Ui()
    app.exec_()




